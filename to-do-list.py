def show_intro():
	# Welcome Msg
	customer_name = input("Hi, what's your name? ")

	print("What should we pick up at the store for you {}?".format(customer_name))
	return customer_name

def show_help():
    # print out instructions on how to use the app
    print("""
Enter 'DONE' to stop adding items.
Enter 'HELP' for this help.
Enter 'SHOW' to see your current list.
Enter 'DEL' to delete an item from your list.
""")
    
def show_list(shopping_list):
    # print out the list
    if shopping_list != []:
    	print("Here's your list:")
    
    	for item in shopping_list:
        	print(item)
    else:
    	print("There's nothing on your list, buddy!")
        
def add_to_list(shopping_list, new_item):
    # add new items to our list
    shopping_list.append(new_item)
    print("Added {}. List now has {} items.".format(new_item, len(shopping_list)))
    return shopping_list
    
def remove_from_list(shopping_list):
	# remove item from list
	remove_item = input("What should we remove from your list? ")

	for item in shopping_list:
		if item == remove_item:
			shopping_list.remove(remove_item)
		else:
			continue
	print("We have removed {} from your list.".format(remove_item))		

def main():

	show_intro()	

	# make a list to hold onto our items
	shopping_list = []

	while True:
	    # ask for new items
	    new_item = input("> ")
	    
	    # be able to quit the app
	    if new_item == 'DONE':
	        break
	    elif new_item == 'HELP':
	        show_help()
	        continue
	    elif new_item == 'SHOW':
	        show_list(shopping_list)
	        continue
	    elif new_item == 'DEL':
	    	remove_from_list(shopping_list)
	    	continue

	    add_to_list(shopping_list, new_item)

	show_list(shopping_list)

main()